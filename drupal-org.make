; aeg make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc6"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.2"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.15"
projects[ctools][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[features][version] = "2.11"
projects[features][subdir] = "contrib"

projects[entityreference][version] = "1.5"
projects[entityreference][subdir] = "contrib"

projects[backup_migrate][version] = "3.6"
projects[backup_migrate][subdir] = "contrib"

projects[email_registration][version] = "1.4"
projects[email_registration][subdir] = "contrib"

projects[entity][version] = "1.9"
projects[entity][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.7"
projects[token][subdir] = "contrib"

projects[ckeditor][version] = "1.18"
projects[ckeditor][subdir] = "contrib"

projects[views][version] = "3.22"
projects[views][subdir] = "contrib"

; +++++ Themes +++++

; bootstrap
projects[bootstrap][type] = "theme"
projects[bootstrap][version] = "3.23"
projects[bootstrap][subdir] = "contrib"

